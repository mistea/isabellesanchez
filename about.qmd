---
title: "A propos"
---

This site was created using [Quarto](https://quarto.org){target="_blank"} by [Isabelle Sanchez](https://forgemia.inra.fr/isabelle.sanchez){target="_blank"} (INRAE MISTEA).

The code for this site can be viewed at: [https://forgemia.inra.fr/mistea/isabellesanchez](https://forgemia.inra.fr/mistea/isabellesanchez){target="_blank"}.

© 2024 - GPL3 License - INRAE


