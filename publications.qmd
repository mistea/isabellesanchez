---
title: "Publications"
output: 
  html_document:
   toc: false
   logo: img/Logotype-INRAE.jpg
   number_sections: false
   includes:
     before_body: extlogo.html
---


# HAL CV

[mon CV HAL](https://cv.archives-ouvertes.fr/isabelle-sanchez?langChosen=fr){target="_blank"}
