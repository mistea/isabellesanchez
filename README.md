# site web perso

Site web perso créé en [Quarto](https://quarto.org) et propulsé en CI/CD par gitlab forgemia!

© 2024 - GPL3 License - INRAE

Creation: [@isabelle.sanchez](https://forgemia.inra.fr/isabelle.sanchez) (INRAE MISTEA) .
          
https://mistea.pages.mia.inra.fr/isabellesanchez/


Pour avoir une jolie adresse url concise, n'oubliez pas de décocher l'option "Use unique domain" dans le menu à gauche `Deploy`-> `Pages`

